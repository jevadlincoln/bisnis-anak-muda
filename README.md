# README #

memulai usaha di usia muda bukanlah sesuatu hal yang mudah. Diperlukan semangat dan kemampuan yang tepat untuk bisa memulai usaha yang sukses diusia muda. Namun dengan ide bisnis yang tepat menjadi sukses sebagai pengusaha muda bukan hal yang mustahil.
Untuk memulai anda harus menemukan ide bisnis yang tepat terlebih dahulu. Setelah itu barulah anda memikirkan mengenai masalah modal dan strategi.
Jika anda sedang mencari inspirasi ide bisnis anak muda anda dapat membaca artikel mengenai ide bisnis anak muda di https://idntrepreneur.com/30-ide-bisnis-anak-muda-dengan-modal-kecil.